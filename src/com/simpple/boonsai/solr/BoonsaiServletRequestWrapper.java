package com.simpple.boonsai.solr;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.log4j.Logger;

public class BoonsaiServletRequestWrapper 
    extends HttpServletRequestWrapper {
 
    static final Logger logger = Logger.getLogger(BoonsaiServletRequestWrapper.class);
    
    //Convert to non-finals, and load from a properties file on init to play whith the module
    private static final String queryField = "q";
    private static final String loginField = "login";
    private static final String similarityOperator = "~";
    private static final double defaultSimilarityFraction =  0.67;
    private static final String permissionsField = "permisos";
    
    private String oldQuery;
    private String login;
    private String newQuery;
    private double similarityFraction; 
    

    public BoonsaiServletRequestWrapper(HttpServletRequest request) {
        super(request);
        String dist = this.getRequest().getParameter("dist");
        double d = defaultSimilarityFraction;
        try {
            if (null != dist && dist.trim() != "") {
            
                d = Double.parseDouble(dist);
                logger.info(" dist parameter: "+d);
            }
        } catch (Exception e) {
            logger.info("Error parsing dist parameter");
        } finally {
            similarityFraction=d;
        }            
        
        this.login = this.getRequest().getParameter(loginField);
        this.oldQuery = this.getRequest().getParameter(queryField);
        if (null == login || login.trim().equalsIgnoreCase("")) {
            throw new  RuntimeException("Login parameter empty or not existent");
        }
        //if oldQuery is xxx  then newQuery =  xxx~0.8 +permisos:this.login
        this.newQuery = this.oldQuery+similarityOperator+similarityFraction+" "+permissionsField+":"+this.login; 
    }

    public String getParameter(String paramName) {
        String ret =  null;
        if (paramName.equals(queryField)) {
            ret= this.newQuery;
        } else {
            ret = super.getParameter(paramName);
        }
        return ret;
    }

    public Map<String, String[]> getParameterMap() {
        Map<String, String[]> map = super.getParameterMap();
        Map<String, String[]> newMap = new HashMap<String, String[]>(map);
        newMap.put(queryField, new String[] {newQuery});
        return  newMap;
    }


    public String[] getParameterValues(String paramName) {
        String[] ret =  null;
        if (paramName.equals(queryField)) {
            ret= new String[]{ this.newQuery };
        } else {
            ret = super.getParameterValues(paramName);
        }
        return ret;
    }

}
