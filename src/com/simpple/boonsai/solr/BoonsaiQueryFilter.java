package com.simpple.boonsai.solr;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * This class must be called before request made on solr/select.
 *  It gets the params "q" and "userId" from request and modify
 *  q (the query)  
 *    
 * 
 * @author Pere Cortada
 *
 */
public final class BoonsaiQueryFilter implements Filter {

    static Logger logger = Logger.getLogger(BoonsaiQueryFilter.class);
            

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        
        ServletRequest req =
            new BoonsaiServletRequestWrapper(
                    (HttpServletRequest)request
            );
        
        chain.doFilter(req, response);
    }

    
    public void init(FilterConfig arg0) throws ServletException {
    }        
    
    public void destroy() {
    }
}