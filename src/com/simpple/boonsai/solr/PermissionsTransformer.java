package com.simpple.boonsai.solr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PermissionsTransformer {
    static final Logger logger = Logger.getLogger(PermissionsTransformer.class);
    
    private static final String page = "http://" + Entorn.getServidorBoonsaiOI() +":"+ Entorn.getPortservidorBoonsaiOI() +"/"+
    										Entorn.getNomBoonsaiOIDesplegat() + "/search/permisos";
    
    public Object transformRow(Map<String,Object>row) throws Exception {
    	System.out.println("*-*-*Init transformation process*-*-*");
//    	imprimirMap(row);
    	row = lowerCase(row);
    	System.out.println("*-*-*ROW "+row.toString());
    	
    	String jsonArrayAsString;
    	JSONArray jsarr;
    	ArrayList<String> values = new ArrayList<String>();
        String url="";
       
        int workstatus = 100;
        if (row.get("work_status_id")!= null){
        	workstatus = Integer.parseInt(row.get("work_status_id").toString());
        }
        
        System.out.println("**Workstatus "+workstatus);
        if(workstatus == 3  || workstatus == 2){ 
        	//son historicos o borrados logicos
        }else{
        	
	    	url = getUrl(row); 
	    	System.out.println("**URL "+url);
	
	        jsonArrayAsString = getHttpPage(url);

	        try {
	        	 jsarr = new JSONArray(jsonArrayAsString);
		        for (int j = 0; j < jsarr.length(); j++) {
		            values.add(String.valueOf(jsarr.get(j)));
		        }
	        } catch (Exception e){
	        	System.out.println("Error: " + e.getMessage());
	        }
	        row.put("permisos", values);
	        System.out.println("**permisos "+values.size());
	        //procesar la url bien
        }
        return row;
    }
    
    private Map<String,Object> lowerCase(Map<String,Object>row) throws JSONException {
    	Map<String,Object> row2 = new HashMap<String,Object>();
    	Iterator<?> it = row.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            String key = ((String)e.getKey()).toLowerCase();
            if (row2.containsKey(key) && e.getValue()==null){
            	
            }else{
            	row2.put(((String)e.getKey()).toLowerCase(), e.getValue());
            }
//            todo += " ["+e.getKey() + "=" + e.getValue()+"] ";
        }
       return row2;
    }
    
    private void imprimirMap(Map<String,Object>row) throws JSONException {
    	Map<String,Object> row2 = new HashMap<String,Object>();
    	Iterator<?> it = row.entrySet().iterator();
    	String todo="";
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            row2.put(((String)e.getKey()).toLowerCase(), e.getValue());
            todo += " ["+e.getKey() + "=" + e.getValue()+"] ";
        }
        System.out.println("MAP: "+todo);
    }
    
    
    private String getUrl(Map<String,Object>row) throws Exception{
    	String url = "";
    	try{

	    	 imprimirMap(row);
	    	 String abr = (String)row.get("abr");
	    	 String id, parentId;
		     System.out.println("ABR: "+abr);
	    	 
	    	 
	    	// Si la entitat  per la qual es sol.liciten permisos, es una entitat amb germans (p.e. procediments,
	 		// indicadors, noconformitats...), se'ls hi assignaran els permisos que té el node pare d'aquestes,
	    	// donat que dites entitats no disposen de permisos propis,
	    	
	    	 if (("IND".equalsIgnoreCase(abr) || ("PRD".equalsIgnoreCase(abr)))){
	    		 parentId = getParentIdStr(row,"parent_id");
	         	// Per a indicador i procediments construim la url amb l'id del pare i l'abreviatura de Processos
	 			url = page + "?object_identifier=" + parentId + "&table_identifier=PRC";
	 			
	 		} else 	if ( "pos_resp".equalsIgnoreCase(abr) ){
	 			parentId = getParentIdStr(row,"parent_id");
	    		url = page + "?object_identifier=" + parentId + "&table_identifier=LLC";
	    		
	    	}else	if ( "temps".equalsIgnoreCase(abr) ||  "compres".equalsIgnoreCase(abr)
	    			|| "risks".equalsIgnoreCase(abr)){
	    		if ( "temps".equalsIgnoreCase(abr)){
	    			parentId = getParentIdStr(row,"parent_id");
	    		}
	    		else{
	    			parentId = getParentIdStr(row,"objective_id");
	    		}
	    		
	    		
	    		url = page + "?object_identifier=" + parentId + "&table_identifier=PLA";
	    		
	    		if(Integer.parseInt(parentId) == 7495){
	    			System.out.println("URL: "+ url +" id "+row.get("id")+ "coleccio "+row.get("coleccio"));
	    		}
	    	}
	 		else{
	    		id = getIdStr(row);
	    		System.out.println("**ID "+id);
	 			url = page + "?object_identifier=" + id + "&table_identifier=" + abr;
	    	}
    	}catch(Exception e){
    		System.out.println("Exception "+e.getMessage());
    		throw new Exception(e.getMessage());
    	}
//    	 System.out.println("GetUrl: " + url );
    	return url;
    }
    
    private String getIdStr(Map<String,Object>row){
    	Object obj = row.get("id");
   
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Long) {
            return ((Long) obj).toString();
        }
        if (obj instanceof Integer) {
            return ((Integer) obj).toString();
        }
        if (obj instanceof BigDecimal){
        	 return ((BigDecimal) obj).toString();
        }else {
        	return "";
        }
    } 
    
    private String getParentIdStr(Map<String,Object>row, String nameField){
    	Object obj = row.get(nameField);
       
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Long) {
            return ((Long) obj).toString();
        }
        if (obj instanceof Integer) {
            return ((Integer) obj).toString();
        }
        if (obj instanceof BigDecimal){
        	return ((BigDecimal) obj).toString();
        }
        else {
        	return "";
        }
    }
    
    private JSONArray mockJsonArrayString(String[] permisos) throws JSONException {
        
            JSONArray jsonArray = new JSONArray();            
            for (int i = 0; i < permisos.length; i++) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("login", permisos[i]);
                jsonArray.put(jsonObj);
            }
            return jsonArray;
        
    }

    public String getHttpPage(String urlString) {
        String page = null;
        URL url = null;
        try {
        	System.out.println("getHttpPage.urlString: " + urlString); 
            url = new URL(urlString);
       } catch (MalformedURLException e) {
    	   System.out.println("Error in URL syntax: "+urlString);
           throw new  RuntimeException(e);
       }
        
        int responseCode = 0;
        HttpURLConnection con = null;
        try {
//        	System.out.println("getHttpPage.obrint_conexio_url");
             con = (HttpURLConnection) url.openConnection();
//             System.out.println("getHttpPage_conexio_url_oberta");
             
//             System.out.println("getHttpPage.agafant response code");
             responseCode = con.getResponseCode();
//             System.out.println("getHttpPage.responseCode: " + urlString);
             if (responseCode != 200  && responseCode != 302) {
            	 System.out.println( "Error: "+responseCode);
                 throw new RuntimeException();
             }            
             
//             System.out.println("getHttpPage.entrem getPage(con)");
             page = getPage(con);
             System.out.println("getHttpPage.sortim fet getPage(con): " + page);
        } catch (IOException e) {
        	System.out.println("Error accessing network ");
            throw new  RuntimeException(e);
        }finally{
        	con.disconnect();
        }
        return page;
    }
    
    
    
    private String getPage(HttpURLConnection con) {
        StringBuilder output = new StringBuilder();
        try {

//        	System.out.println("getPage.entrem a llegir buffer");
            // Here BufferedReader is added for fast reading.
            BufferedReader bis =
                new BufferedReader(
                        new InputStreamReader(
                                con.getInputStream()
                                ) 
                        );

            //  bis.readLine() returns 0 if the file does not have more lines.
            String line = bis.readLine();
            while (null != line ) {
                output.append(line);
                line = bis.readLine();
            }
//            System.out.println("getPage.sortim de llegir buffer");
        } catch (IOException e) {
            throw new  RuntimeException(e);
        }            
        return output.toString();
    }
    
    
//    public static void main(String[] args) {
//        Map<String,Object>row = new HashMap<String,Object>();
//        PermissionsTransformer peta = new PermissionsTransformer();
//        try {
//            peta.transformRow(row);
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        
//        
//    }
}
