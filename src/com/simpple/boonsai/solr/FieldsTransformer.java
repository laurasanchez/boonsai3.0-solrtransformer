package com.simpple.boonsai.solr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;


public class FieldsTransformer {
    static final Logger logger = Logger.getLogger(PermissionsTransformer.class);
    
    private static final String page = "http://" + Entorn.getServidorBoonsaiOI() +":"+ Entorn.getPortservidorBoonsaiOI() +"/"+
    										Entorn.getNomBoonsaiOIDesplegat() + "/search/fields";
    
    public Object transformRow(Map<String,Object>row) throws Exception {
    	System.out.println("*-*-*Init transformation process*-*-*");

    	row = lowerCase(row);
    	System.out.println("*-*-*ROW "+row.toString());
    	
    	String jsonArrayAsString;
    	JSONArray jsarr;
    	ArrayList<String> values = new ArrayList<String>();
        String url="";
       
        int workstatus = 100;
        if (row.get("work_status_id")!= null){
        	workstatus = Integer.parseInt(row.get("work_status_id").toString());
        }
        
        System.out.println("**Workstatus "+workstatus);
        if(workstatus == 3  || workstatus == 2){ 
        	//son historicos o borrados logicos
        }else{
        	//en este caso, queremos holder y responsable
    		url = page +"?manager="+row.get("manager_id");
	    	System.out.println("**URL "+url);
	
	        jsonArrayAsString = getHttpPage(url);

	        try {
	        	 jsarr = new JSONArray(jsonArrayAsString);
		        for (int j = 0; j < jsarr.length(); j++) {
		            values.add(String.valueOf(jsarr.get(j)));
		        }
	        } catch (Exception e){
	        	System.out.println("Error: " + e.getMessage());
	        }
	        row.put("responsable_name", values);
	        System.out.println("=======row1 "+row.get("name1"));
        	if (row.get("name1")!= null){
        		values = new ArrayList<String>();
        		//en este caso, queremos holder y responsable
        		url = page +"?holder_login_name="+row.get("login");
    	    	System.out.println("**URL "+url);
    	
    	        jsonArrayAsString = getHttpPage(url);

    	        try {
    	        	 jsarr = new JSONArray(jsonArrayAsString);
    		        for (int j = 0; j < jsarr.length(); j++) {
    		            values.add(String.valueOf(jsarr.get(j)));
    		        }
    	        } catch (Exception e){
    	        	System.out.println("Error: " + e.getMessage());
    	        }
    	        row.put("holder_login_name", values);

        	}else{
        		if (row.get("check_acum_12")!= null){//problemas con dashboards
        			values = new ArrayList<String>();
            		//en este caso, queremos holder y responsable
            		url = page +"?dashboard="+row.get("id");
        	    	System.out.println("**URL "+url);
        	
        	        jsonArrayAsString = getHttpPage(url);

        	        try {
        	        	 jsarr = new JSONArray(jsonArrayAsString);
        		        for (int j = 0; j < jsarr.length(); j++) {
        		            values.add(String.valueOf(jsarr.get(j)));
        		        }
        	        } catch (Exception e){
        	        	System.out.println("Error: " + e.getMessage());
        	        }
        	        row.put("dashboard_name", values);
        			
        		}
        	}
	    	
        }
        return row;
    }
    
    private Map<String,Object> lowerCase(Map<String,Object>row) throws JSONException {
    	Map<String,Object> row2 = new HashMap<String,Object>();
    	Iterator<?> it = row.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            String key = ((String)e.getKey()).toLowerCase();
            if (row2.containsKey(key) && e.getValue()==null){
            	
            }else{
            	row2.put(((String)e.getKey()).toLowerCase(), e.getValue());
            }
//            todo += " ["+e.getKey() + "=" + e.getValue()+"] ";
        }
       return row2;
    }


    public String getHttpPage(String urlString) {
        String page = null;
        URL url = null;
        try {
        	System.out.println("getHttpPage.urlString: " + urlString); 
            url = new URL(urlString);
       } catch (MalformedURLException e) {
    	   System.out.println("Error in URL syntax: "+urlString);
           throw new  RuntimeException(e);
       }
        
        int responseCode = 0;
        HttpURLConnection con = null;
        try {
//        	System.out.println("getHttpPage.obrint_conexio_url");
             con = (HttpURLConnection) url.openConnection();
//             System.out.println("getHttpPage_conexio_url_oberta");
             
//             System.out.println("getHttpPage.agafant response code");
             responseCode = con.getResponseCode();
//             System.out.println("getHttpPage.responseCode: " + urlString);
             if (responseCode != 200  && responseCode != 302) {
            	 System.out.println( "Error: "+responseCode);
                 throw new RuntimeException();
             }            
             
//             System.out.println("getHttpPage.entrem getPage(con)");
             page = getPage(con);
             System.out.println("getHttpPage.sortim fet getPage(con): " + page);
        } catch (IOException e) {
        	System.out.println("Error accessing network ");
            throw new  RuntimeException(e);
        }finally{
        	con.disconnect();
        }
        return page;
    }
    
    
    
    private String getPage(HttpURLConnection con) {
        StringBuilder output = new StringBuilder();
        try {

//        	System.out.println("getPage.entrem a llegir buffer");
            // Here BufferedReader is added for fast reading.
            BufferedReader bis =
                new BufferedReader(
                        new InputStreamReader(
                                con.getInputStream()
                                ) 
                        );

            //  bis.readLine() returns 0 if the file does not have more lines.
            String line = bis.readLine();
            while (null != line ) {
                output.append(line);
                line = bis.readLine();
            }
//            System.out.println("getPage.sortim de llegir buffer");
        } catch (IOException e) {
            throw new  RuntimeException(e);
        }            
        return output.toString();
    }
    
    
//    public static void main(String[] args) {
//        Map<String,Object>row = new HashMap<String,Object>();
//        PermissionsTransformer peta = new PermissionsTransformer();
//        try {
//            peta.transformRow(row);
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        
//        
//    }
}
