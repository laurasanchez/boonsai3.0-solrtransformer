package com.simpple.boonsai.solr;

import java.io.IOException;
import javax.servlet.ServletContext;
import org.apache.log4j.Logger;

/**
 * 
 * @author dpamies - Simpple
 * @author Eduard Paun&eacute; - Simpple
 * 2006-2008 � Simpple. Tots els drets reservats
 */

public class Entorn {
	// logger
	static final Logger logger = Logger.getLogger(Entorn.class.getName());

	
	private static ServletContext servletContext = null;
	
	public static boolean carregat = false;
	
	private static String servidorBoonsaiOI ="localhost";
	private static String portservidorBoonsaiOI = "8080";			
	private static String nomBoonsaiOIDesplegat = "Boonsai3.0";
	
	  
	/**
	/**
	 * Carrega la configuracio del fitxer de propietats, carrega l'idioma, es connecta a la BD, i l'actualitza si cal
	 * @throws IOException 
	 * @throws  
	 */
	public static void carrega() throws Exception {
		
		logger.debug("Carregant entorn...");
		
		if (servletContext == null) {
			logger.debug("ServletContext null, les variables ...");
//			carregaAtributsTestDesdeFitxer();
			
			// Si estem executant JUnit, hem de simular el "hostedMode"
//			hostedMode = true;
			
		} else {
			if (servletContext.getInitParameter("servidorBoonsaiOI") != null) {
				servidorBoonsaiOI = servletContext.getInitParameter("servidorBoonsaiOI");
				logger.debug("servidorBoonsaiOI=" + servidorBoonsaiOI);
			} else {
				logger.debug(">> Parametre servidorBoonsaiOI no esta al context. S'agafa el valor per defecte: servidorBoonsaiOI= " + servidorBoonsaiOI);
			}
			if (servletContext.getInitParameter("portservidorBoonsaiOI") != null) {
				portservidorBoonsaiOI = servletContext.getInitParameter("portservidorBoonsaiOI");
				logger.debug("portservidorBoonsaiOI=" + portservidorBoonsaiOI);
			} else {
				logger.debug(">> Parametre portservidorBoonsaiOI no esta al context. S'agafa el valor per defecte: portservidorBoonsaiOI= " + portservidorBoonsaiOI);
			}
			if (servletContext.getInitParameter("nomBoonsaiOIDesplegat") != null) {
				nomBoonsaiOIDesplegat = servletContext.getInitParameter("nomBoonsaiOIDesplegat");
				logger.debug("nomBoonsaiOIDesplegat=" + nomBoonsaiOIDesplegat);
			} else {
				logger.debug(">> Parametre nomBoonsaiOIDesplegat no esta al context. S'agafa el valor per defecte: nomBoonsaiOIDesplegat= " + nomBoonsaiOIDesplegat);
			}
			
		}
		
		carregat = true;
		
		logger.info("Configuracio carregada");
		logger.info("Parametres de configuracio: " + servidorBoonsaiOI + " %% " + portservidorBoonsaiOI+ " %% " + nomBoonsaiOIDesplegat + " %% " );
		
	}


	/**
	 * @return the servletContext
	 */
	public static ServletContext getServletContext() {
		return servletContext;
	}


	/**
	 * @param servletContext the servletContext to set
	 */
	public static void setServletContext(ServletContext servletContext) {
		Entorn.servletContext = servletContext;
	}


	/**
	 * @return the servidorBoonsaiOI
	 */
	public static String getServidorBoonsaiOI() {
		return servidorBoonsaiOI;
	}


	/**
	 * @param servidorBoonsaiOI the servidorBoonsaiOI to set
	 */
	public static void setServidorBoonsaiOI(String servidorBoonsaiOI) {
		Entorn.servidorBoonsaiOI = servidorBoonsaiOI;
	}


	/**
	 * @return the portservidorBoonsaiOI
	 */
	public static String getPortservidorBoonsaiOI() {
		return portservidorBoonsaiOI;
	}


	/**
	 * @param portservidorBoonsaiOI the portservidorBoonsaiOI to set
	 */
	public static void setPortservidorBoonsaiOI(String portservidorBoonsaiOI) {
		Entorn.portservidorBoonsaiOI = portservidorBoonsaiOI;
	}


	/**
	 * @return the nomBoonsaiOIDesplegat
	 */
	public static String getNomBoonsaiOIDesplegat() {
		return nomBoonsaiOIDesplegat;
	}


	/**
	 * @param nomBoonsaiOIDesplegat the nomBoonsaiOIDesplegat to set
	 */
	public static void setNomBoonsaiOIDesplegat(String nomBoonsaiOIDesplegat) {
		Entorn.nomBoonsaiOIDesplegat = nomBoonsaiOIDesplegat;
	}
	
	
	
	/**
	 * Carrega els atributs de l'aplicació des del fitxer de properties.
	 */
//	private static void carregaAtributsTestDesdeFitxer()
//	throws IOException, DataAccessException{
//		
//		try {
//			props = new Properties();
//			File propertiesFile = new File("boonsai_test.conf");
//			InputStream inputStream = new FileInputStream(propertiesFile);
//			props.load(inputStream);
//			
//			if (props.getProperty("idioma")!=null) idioma = props.getProperty("idioma");
//			if (props.getProperty("tipus")!=null) tipus = props.getProperty("tipus");
//			if (props.getProperty("user")!=null)  user = props.getProperty("user");
//			if (props.getProperty("clau")!=null) clau = props.getProperty("clau");
//			if (props.getProperty("servidorBD")!=null) servidorBD = props.getProperty("servidorBD");
//			if (props.getProperty("servidorWeb")!=null) servidorWeb = props.getProperty("servidorWeb");
//			if (props.getProperty("portServidorWeb")!=null) portServidorWeb = props.getProperty("portServidorWeb");
//			if (props.getProperty("pathServidorInformesBirt")!=null) pathServidorInformesBirt = props.getProperty("pathServidorInformesBirt");
//			if (props.getProperty("pathServidorInformesJasper")!=null) pathServidorInformesJasper = props.getProperty("pathServidorInformesJasper");
//			if (props.getProperty("planificacioEditable")!=null) planificacioEditable = props.getProperty("planificacioEditable").equalsIgnoreCase("true");
//			if (props.getProperty("nomBD")!=null) nomBD = props.getProperty("nomBD");
//			if (props.getProperty("userBD")!=null) userBD = props.getProperty("userBD");
//			if (props.getProperty("passwordBD")!=null) passwordBD = props.getProperty("passwordBD");
//			if (props.getProperty("motorBD")!=null) motorBD = props.getProperty("motorBD");
//			if (props.getProperty("estructura") != null) estructura = props.getProperty("estructura");
//			
//			inputStream.close();
//			
//		} catch (FileNotFoundException flne){
//			throw new DataAccessException(LicenseMessages.FITXER_WEB_XML_CORRUPTE);
//		}
//	}
	
}