package com.simpple.boonsai.solr;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class InitPropertyListener implements ServletContextListener {
	
	private final static Logger logger = Logger.getLogger(InitPropertyListener.class);

	ServletContext servletContext;
	
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		//servletContext.removeAttribute("boonsai.properties.path");
		//servletContext.removeAttribute("boonsai.i18n.path");
	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {

		logger.debug("Inicialitzant context...");
		
		System.out.println("Inicialitzant context");
		
		servletContext = servletContextEvent.getServletContext();
		
		Entorn.setServletContext(servletContext);
		
		try {
			Entorn.carrega();
			logger.debug("Context inicialitzat!");
		} catch (Exception e) {
			logger.error("Context NO inicialitzat!", e);
			
		}
		
		
	}
	
}
